﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OctopusBossController : MonoBehaviour
{
    private int state = 0;
    public Transform playerTransform;
    public Transform cameraTransform;
    public Transform far;
    public Transform near;
    public Transform last;
    public Transform tentacle1;
    public Transform tentacle2;
    public Transform tentacle3;
    public Transform tentacle4;

    void Update()
    {
        if (state == 0)
        {
            far.LookAt(playerTransform);
        }
        if (state == 2)
        {
            near.LookAt(playerTransform);
            near.position = new Vector3(cameraTransform.position.x, near.position.y, 115);
        }
        if (state == 4)
        {
            last.position = new Vector3(cameraTransform.position.x + 25.5f, last.position.y, 0);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (state == 0)
            {
                state = 1;
                StartCoroutine("TransitionToState2");
                GameObject.Find("SoundEffectSpeaker").GetComponent<SoundEffectScript>().playOneShotAudio(7, 1);
            }
            if (state == 2)
            {
                state = 3;
                StartCoroutine("TransitionToState4");
                GameObject.Find("SoundEffectSpeaker").GetComponent<SoundEffectScript>().playOneShotAudio(12, 1);
            }
        }
    }

    private IEnumerator TransitionToState2()
    {
        far.DOMoveY(-120, 4);
        yield return new WaitForSeconds(3);
        near.gameObject.SetActive(true);
        state = 2;
    }

    private IEnumerator TransitionToState4()
    {
        near.DOMoveY(-120, 3);
        yield return new WaitForSeconds(2);
        last.gameObject.SetActive(true);
        state = 4;
        StartCoroutine("TentacleAttack");
    }

    private IEnumerator TentacleAttack()
    {
        yield return new WaitForSeconds(0.5f);
        tentacle1.DORotate(new Vector3(0, 0, -220), 3.2f, RotateMode.WorldAxisAdd);
        yield return new WaitForSeconds(2.8f);
        tentacle2.DORotate(new Vector3(0, 0, 220), 3f, RotateMode.WorldAxisAdd);
        yield return new WaitForSeconds(2.8f);
        tentacle1.DORotate(new Vector3(0, 0, 220), 2.4f, RotateMode.WorldAxisAdd);
        GameObject.Find("GameManager").GetComponent<GameManager>().PlayScreenFlash();
        yield return new WaitForSeconds(2f);
        tentacle1.DORotate(new Vector3(0, 0, -220), 2.2f, RotateMode.WorldAxisAdd);
        yield return new WaitForSeconds(1.0f);
        tentacle3.DORotate(new Vector3(0, 0, -220), 2.0f, RotateMode.WorldAxisAdd);
        yield return new WaitForSeconds(0.6f);
        tentacle4.DORotate(new Vector3(0, 0, -220), 1.8f, RotateMode.WorldAxisAdd);
        yield return new WaitForSeconds(2.0f);
        GameObject.Find("SoundEffectSpeaker").GetComponent<SoundEffectScript>().playOneShotAudio(1, 0.5f);
        GameObject.Find("DarkModeController").GetComponent<DarkModeController>().TurnToNormalMode();

    }
}
