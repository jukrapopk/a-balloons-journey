﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Movement Info")]
    private float x = 0.0f;
    private float y = 0.0f;

    public float movementSpeed = 0.0f;

    private bool isAbleToControl;

    public Rigidbody rb;

    private void Start()
    {
        isAbleToControl = true;
    }

    private void Update()
    {
        x = Input.GetAxis("Horizontal") * movementSpeed * Time.fixedDeltaTime;
        y = Input.GetAxis("Vertical") * movementSpeed * Time.fixedDeltaTime;
    }

    void FixedUpdate()
    {
        if (isAbleToControl)
        {
            rb.velocity = new Vector3(x, y, 0);
        }
    }

    public void OnExplosion()
    {
        isAbleToControl = false;
        GetComponent<Rigidbody>().useGravity = true;
    }
}
