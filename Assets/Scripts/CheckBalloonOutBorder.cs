﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckBalloonOutBorder : MonoBehaviour
{
    // Start is called before the first frame update
    public float limitTime = 3f;
    public SpriteRenderer redAlard = null;
    private float timeCounter = 0.0f;
    private float alpha = 0;
    private bool isOut = false;
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.layer == 8)
        {
            isOut = true;
            timeCounter += Time.deltaTime;
            alpha = timeCounter / limitTime;

            redAlard.color = new Color(1, 1, 1, alpha);

            if (timeCounter >= limitTime)
            {
                other.gameObject.GetComponent<BalloonCollision>().boom();
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            isOut = false;
        }
    }
    private void Update()
    {
        if (!isOut)
        {
            if(timeCounter > 0)
            {
                timeCounter -= Time.deltaTime;
                alpha = timeCounter / limitTime;
                if(timeCounter <= 0)
                {
                    alpha = 0;
                }
                redAlard.color = new Color(1, 1, 1, alpha);
            }
        }
    }
}
