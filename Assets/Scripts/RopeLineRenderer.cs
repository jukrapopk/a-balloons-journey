﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeLineRenderer : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public Transform[] vertices;

    private void Start()
    {
        lineRenderer.positionCount = vertices.Length;
    }

    private void LateUpdate()
    {
        SetLineRenderer();
    }

    private void SetLineRenderer()
    {
        if (vertices.Length > 1)
        {
            for (int i = 1; i < vertices.Length; i++)
            {
                lineRenderer.SetPosition(i - 1, vertices[i - 1].position);
                lineRenderer.SetPosition(i, vertices[i].position);
            }
        }
    }

}
