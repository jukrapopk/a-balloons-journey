﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSourceControllerScript : MonoBehaviour
{
    public AudioClip[] BGMusic;
    private AudioSource speaker;
    void Start()
    {
        speaker = GetComponent<AudioSource>();
        //DontDestroyOnLoad(gameObject);
        speaker.playOnAwake = false;
    }

    public void ChangeAudioClip(int clipNumber)
    {
        speaker.clip = BGMusic[clipNumber];
    }

}
