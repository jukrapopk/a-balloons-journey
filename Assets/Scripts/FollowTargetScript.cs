﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTargetScript : MonoBehaviour
{
    // Start is called before the first frame update
    [Header("Target Info")]
    public Transform target = null;

    [Header("General Info")]
    public float movementSpeed = 0.0f;

    [Header("Following Script Info")]
    public float limitTime = 0.0f;
    private float timeTicker = 0.0f;
    private bool isFollowing = true;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        checkFollowing();
        if (isFollowing)
        {
            transform.LookAt(target);
        }
        transform.position += transform.forward * movementSpeed * Time.deltaTime;
    }

    void checkFollowing()
    {
        timeTicker += Time.deltaTime;
        if(timeTicker >= limitTime)
        {
            isFollowing = false;
        }
    }
    private void OnEnable()
    {
        GameObject.Find("SoundEffectSpeaker").GetComponent<SoundEffectScript>().playOneShotAudio(Random.Range(8,12), 0.4f);
    }
}
