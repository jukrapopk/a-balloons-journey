﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeTextEndScene : MonoBehaviour
{
    public Animation endText = null;
    void Start()
    {
        Invoke("playFade", endText.GetClip("OpenText").length);
    }
    public void playFade()
    {
        GetComponent<Animation>().Play();
    }
}
