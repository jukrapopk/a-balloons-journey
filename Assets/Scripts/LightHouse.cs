﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightHouse : MonoBehaviour
{
    public float spinSpeed = 1;
    public GameObject pivot;
    public bool shouldSpin = true;

    void FixedUpdate()
    {
        if (shouldSpin)
        {
            pivot.transform.Rotate(new Vector3(0, spinSpeed * Time.fixedDeltaTime, 0));
        }
    }
}
