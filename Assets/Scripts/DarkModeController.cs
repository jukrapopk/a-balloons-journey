﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkModeController : MonoBehaviour
{
    public GameManager gameManager;

    public GameObject backDrop;
    public Light directionalLight;
    public MeshRenderer redRenderer;
    public MeshRenderer balloonRenderer;
    public Material balloonMaterial;
    public Material balloonInDarkMaterial;
    public Material redMaterial;
    public Material redInDarkMaterial;
    public GameObject playerPropeller;
    public ConstantForce balloonForce;
    public GameObject stormParticleEffect;

    public LightHouse lightHouse;
    public GameObject flashLight;
    private AudioSource speaker = null;
    private GameObject mainCamera = null;

    private int state = 0;

    private void Start()
    {
        state = 0;
        if (GameObject.Find("Speaker").GetComponent<AudioSource>() != null)
        {
            speaker = GameObject.Find("Speaker").GetComponent<AudioSource>();
        }
        mainCamera = GameObject.Find("Main Camera");
    }

    private IEnumerator TurnToDarkMode()
    {
        stormParticleEffect.SetActive(true);
        GameObject.Find("SoundEffectSpeaker").GetComponent<SoundEffectScript>().playOneShotAudio(0, 0.02f);
        yield return new WaitForSeconds(6.5f);
        gameManager.PlayScreenFlash();
        yield return new WaitForSeconds(2.0f);
        gameManager.PlayScreenFlash();
        AdjustToDarkMode();
        if(speaker != null)
        {
            speaker.GetComponent<AudioSourceControllerScript>().ChangeAudioClip(1);
            speaker.Play();
            GameObject.Find("SoundEffectSpeaker").GetComponent<SoundEffectScript>().playOneShotAudio(1,0.5f);
        }
        mainCamera.GetComponent<PlayerCamera>().scrollingSpeed = 15f;
    }

    public void TurnToNormalMode()
    {
        gameManager.PlayScreenFlash();
        stormParticleEffect.SetActive(false);
        AdjustToNormalMode();
    }

    private void AdjustToDarkMode()
    {
        backDrop.SetActive(true);
        playerPropeller.SetActive(false);
        directionalLight.intensity = 0;
        RenderSettings.ambientIntensity = 0.0f;
        RenderSettings.fogStartDistance = 70.0f;
        RenderSettings.fogEndDistance = 600.0f;
        if (balloonRenderer)
        {
            balloonRenderer.material = balloonInDarkMaterial;
        }
        var mats = redRenderer.materials;
        mats[3] = redInDarkMaterial;
        redRenderer.materials = mats;
        balloonForce.force = new Vector3(-720, -80);
        lightHouse.shouldSpin = true;
        flashLight.SetActive(true);
        state = 2;
    }

    private void AdjustToNormalMode()
    {
        backDrop.SetActive(false);
        playerPropeller.SetActive(true);
        directionalLight.intensity = 1;
        RenderSettings.ambientIntensity = 1.0f;
        RenderSettings.fogStartDistance = 311.27f;
        RenderSettings.fogEndDistance = 430.76f;
        if (balloonRenderer)
        {
            balloonRenderer.material = balloonMaterial;
        }
        var mats = redRenderer.materials;
        mats[3] = redMaterial;
        redRenderer.materials = mats;
        balloonForce.force = new Vector3(-50, 200);
        lightHouse.shouldSpin = true;
        flashLight.SetActive(false);
        state = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (state == 0)
            {
                StartCoroutine(TurnToDarkMode());
                state = 1;
            }
            if (state == 2)
            {
                AdjustToNormalMode();
            }
        }
    }
}
