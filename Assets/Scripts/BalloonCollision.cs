﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonCollision : MonoBehaviour
{
    public GameObject balloon;
    public ConstantForce balloonForce;
    public PlayerController playerController;
    public ExplodedBalloon explosionPrefab;
    public GameObject blockMovementCollider;
    private bool isExploded;
    private PlayerCamera mainCamera = null;
    private ChangeSceneScript respawn = null;
    private AudioSource soundEffect = null;

    public void Start()
    {
        mainCamera = GameObject.Find("Main Camera").GetComponent<PlayerCamera>();
        if(GameObject.Find("ChangeScenePoint").GetComponent<ChangeSceneScript>() != null)
        {
            respawn = GameObject.Find("ChangeScenePoint").GetComponent<ChangeSceneScript>();
        }
        soundEffect = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy") && !isExploded)
        {
            isExploded = true;
            Destroy(balloon);
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            balloonForce.enabled = false;
            blockMovementCollider.SetActive(false);
            playerController?.OnExplosion();
            soundEffect.Play();
        }
    }
    public void boom()
    {
        if (!isExploded)
        {
            isExploded = true;
            Destroy(balloon);
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            balloonForce.enabled = false;
            blockMovementCollider.SetActive(false);
            playerController?.OnExplosion();
            soundEffect.Play();
        }
    }
    public void Update()
    {
        if (isExploded)
        {
            if(mainCamera.scrollingSpeed > 0)
            {
                mainCamera.scrollingSpeed -= Time.deltaTime*2;
            }
            else
            {
                mainCamera.scrollingSpeed = 0;
                if(respawn != null)
                {
                    respawn.respawn();
                }
            }
        }
    }
}
