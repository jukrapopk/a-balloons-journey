﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject canvas;
    public Animation imageAnimation;

    private void Start()
    {
        canvas.SetActive(true);
        PlayScreenFlash();
        GameObject.Find("SoundEffectSpeaker").GetComponent<SoundEffectScript>().playOneShotAudio(2, 1);
    }

    public void PlayScreenFlash()
    {
        imageAnimation.Play();
    }
}
