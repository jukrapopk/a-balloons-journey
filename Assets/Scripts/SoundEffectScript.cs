﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectScript : MonoBehaviour
{
    public AudioClip[] soundEffects;
    public void playOneShotAudio(int audioNumber, float volume)
    {
        GetComponent<AudioSource>().PlayOneShot(soundEffects[audioNumber], volume);
    }
}
