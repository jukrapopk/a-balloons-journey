﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    [Header("Scrolling Info")]
    public float scrollingSpeed = 5.0f;

    void Update()
    {
        transform.position += new Vector3(scrollingSpeed * Time.deltaTime, 0, 0);
    }
}
