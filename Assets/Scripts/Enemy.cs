﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private float enemyActiviatingPoint = 10;
    private float enemyActiviatingPointGlobal = 10;
    [SerializeField]
    private float movementDurarion = 1;
    [SerializeField]
    private Vector3 destination = Vector3.zero;
    private Vector3 destinationGlobal = Vector3.zero;

    private bool isActivated = false;
    private GameObject player;
    private FollowTargetScript followScript = null;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(new Vector3(transform.position.x - enemyActiviatingPoint, 30, 0), new Vector3(transform.position.x - enemyActiviatingPoint, -30, 0));
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position + destination, 1f);
    }

    void Start()
    {
        player = GameObject.FindWithTag("Player");
        enemyActiviatingPointGlobal = transform.position.x - enemyActiviatingPoint;
        destinationGlobal = destination + transform.position;
        if (GetComponent<FollowTargetScript>() != null)
        {
            followScript = GetComponent<FollowTargetScript>();
        }
    }

    void Update()
    {
        if (!isActivated & player & player.transform.position.x > enemyActiviatingPointGlobal)
        {
            if (!isActivated && followScript == null)
            {
                Destroy(gameObject, movementDurarion + 2f);
            }
            isActivated = true;

            if (followScript != null)
            {
                followScript.enabled = true;
                GetComponent<Animator>().SetFloat("AnimationSpeed", 3);
                Destroy(gameObject, 8f);
            }
            else
            {
                transform.DOMove(destinationGlobal, movementDurarion);
            }
        }
    }
}
