﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneScript : MonoBehaviour
{
    public string changeToScene = "";
    public Animation changeSceneAnimation = null;
    bool isActivated = false;

    private void OnTriggerStay(Collider other)
    {

        if (other.tag == "Player")
        {
            if (!isActivated)
            {
                isActivated = true;
                if (changeToScene != null)
                {
                    if (changeSceneAnimation != null)
                    {
                        changeSceneAnimation.Play("TransitionIN");
                        Invoke("changeScene", changeSceneAnimation.GetClip("TransitionIN").length);
                    }
                    else
                    {
                        changeScene();
                    }
                }
                else
                {
                    changeToScene = SceneManager.GetActiveScene().name;
                    if (changeSceneAnimation != null)
                    {
                        changeSceneAnimation.Play("TransitionIN");
                        Invoke("changeScene", changeSceneAnimation.GetClip("TransitionIN").length);
                    }
                    else
                    {
                        changeScene();
                    }
                }
            }
        }
    }
    public void changeScene()
    {
        SceneManager.LoadScene(changeToScene);
    }
    public void respawn()
    {
        changeToScene = SceneManager.GetActiveScene().name;
        if (changeSceneAnimation != null)
        {
            changeSceneAnimation.Play("TransitionIN");
            Invoke("changeScene", changeSceneAnimation.GetClip("TransitionIN").length);
        }
        else
        {
            changeScene();
        }
    }
    public void Update()
    {
        if (SceneManager.GetActiveScene().name == "End")
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                changeScene();
            }
        }
    }
}
