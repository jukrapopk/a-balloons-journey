﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishHerd : MonoBehaviour
{
    public Transform pivot;
    bool isStarted = false;
    public float duration = 3.5f;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (!isStarted)
            {
                pivot.DORotate(new Vector3(0, 0, 300), duration, RotateMode.WorldAxisAdd);
                isStarted = true;
            }
        }
    }
}
