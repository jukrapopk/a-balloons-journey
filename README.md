Jam for "Ludum Dare 46"  
  
Check out: [ldjam.com](https://ldjam.com/events/ludum-dare/46/a-balloons-journey) and [itch.io](https://abook40.itch.io/a-balloons-journey)  
[Teaser on Youtube](https://www.youtube.com/watch?v=-yr8inUlKgU)  
  
Help delivering the letter to the other side of the world.  
The journey through the forest, the ocean, animals and even monsters.  
  
From us “Cabbage Soup Studio”  
  
Somlerk K.  
Jukrapop K.  
Engine: Unity 2019.3.7f1  
Modeling: Blender 2.8  
Sounds and Musics: FreeSound.org, Khinsider  
  
Please enjoy :)  

![coverart](img/2da36.png "Cover")
![screenshots](img/2d978.png "Screenshots")

** Migrated from Unity Collab
